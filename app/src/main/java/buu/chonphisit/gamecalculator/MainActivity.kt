package buu.chonphisit.gamecalculator

import android.graphics.Color
import android.graphics.Color.parseColor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        start()


    }

    private fun start() {
        val number1 = findViewById<TextView>(R.id.numtext1)
        val randomnum = Random.nextInt(0, 10)
        number1.text = randomnum.toString()

        val number2 = findViewById<TextView>(R.id.numtext2)
        val randomnum1 = Random.nextInt(0, 10)
        number2.text = randomnum1.toString()

        val answer = (randomnum + randomnum1)
        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)
        val choice = Random.nextInt(0, 3)

        if (choice == 0) {
            btn1.text = answer.toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer - 1).toString()
        } else if (choice == 1) {
            btn1.text = (answer + 1).toString()
            btn2.text = answer.toString()
            btn3.text = (answer - 1).toString()
        } else {
            btn1.text = (answer - 1).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = answer.toString()

        }

        val pointCorrect = findViewById<TextView>(R.id.pointCorrect)
        val correct: Int = 0
        val pointIncorrect = findViewById<TextView>(R.id.pointIncorrect)
        val incorrect: Int = 0


        btn1.setOnClickListener {
            if (btn1.text.toString().toInt() == answer) {
                ansCorrect(textalert)
                anscorrect(correct)
                start()
                Toast.makeText(this, "Correct", Toast.LENGTH_SHORT).show()
            } else {
                ansIncorrect(textalert)
                ansincorect(incorrect)
                start()
                Toast.makeText(this, "${incorrect} Incorrect", Toast.LENGTH_SHORT).show()
            }
        }
        btn2.setOnClickListener {
            if (btn2.text.toString().toInt() == answer) {
                ansCorrect(textalert)
                anscorrect(correct)
                start()
            } else {
                ansIncorrect(textalert)
                ansincorect(incorrect)
                start()
            }
        }
        btn3.setOnClickListener {
            if (btn3.text.toString().toInt() == answer) {
                ansCorrect(textalert)
                anscorrect(correct)
                start()
            } else {
                ansIncorrect(textalert)
                ansincorect(incorrect)
                start()
            }
        }
    }


    private fun ansincorect(incorrect: Int) {
        pointIncorrect.text = (pointIncorrect.text.toString().toInt()+1).toString()
    }

    private fun anscorrect(correct: Int) {
        pointCorrect.text = (pointCorrect.text.toString().toInt()+1).toString()
    }

    private fun ansIncorrect(textalert: TextView?) {
        textalert!!.text = ("Incorrect")
        textalert.setTextColor(Color.RED)
    }

    private fun ansCorrect(textalert: TextView?) {
        textalert!!.text = ("Correct")
        textalert.setTextColor(Color.GREEN)
    }
}